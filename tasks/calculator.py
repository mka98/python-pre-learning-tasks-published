def calculator(a, b, operator):
    # ==============
    # Your code here
    # ==============
    if operator == "+":
        answer = int(a) + int(b)
    elif operator == "-":
        answer = int(a) - int(b)
    elif operator == "/":
        answer = int(a) / int (b)
    elif operator == "*":
        answer = int(a) * int(b)    
    return answer 

#Code for entering the numbers and operator manually
# ================================================================================
#k = True 
#while k == True:
#    num1 = input("Enter First Number: ")
#    if num1.isdigit():
#        num2 = input("Enter Second Number: ")
#        if num2.isdigit():
#            op = input("Enter Operator + - / *: ")
#            if op == "+":
#                print(calculator(num1, num2, op))                
#            elif op == "-":
#                print(calculator(num1, num2, op))   
#            elif op == "/":
#                print(calculator(num1, num2, op))   
#            elif op == "*":
#                print(calculator(num1, num2, op))   
#            else:
#                print("Enter a valid Operator")
#        else:
#            print("Enter a valid second Number")
#    else:
#        print("Enter a valid Number")
# ================================================================================

print(calculator(2, 4, "+")) # Should print 6 to the console
print(calculator(10, 3, "-")) # Should print 7 to the console
print(calculator(4, 7, "*")) # Should print 28 to the console
print(calculator(100, 2, "/")) # Should print 50 to the console
