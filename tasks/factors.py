def factors(number):
    # ==============
    # Your code here   
    # ==============
    factor_list = []
    for i in range(2, number): #start at 2 because 1 is a factor of everything
        #for loop ends at number-1 automatically so no need to take into account
        #the number itself
       if number % i == 0: 
           #number % retunrs if there is a remainder or not 
           #if no remainder then it is perfectly divisable meaning
           #it is a factor of that number
           factor_list.append(i)   
    return factor_list

print(factors(15)) # Should print [3, 5] to the console
print(factors(12)) # Should print [2, 3, 4, 6] to the console
print(factors(13)) # Should print “[]” (an empty list) to the console
